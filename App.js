// In App.js in a new project

import * as React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AppNavigator from './src/navigation/AppNavigator';
import { NativeBaseProvider} from "native-base";

function App() {
  return (
    <NavigationContainer>
      <NativeBaseProvider>{AppNavigator()}</NativeBaseProvider>
    </NavigationContainer>
  );
}

export default App;
