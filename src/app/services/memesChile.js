const axios = require('axios');

async function getmemes() {
    try {
      const response = await axios.get('https://www.reddit.com/r/chile/new/.json?limit=100');
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  }

export {getmemes};
