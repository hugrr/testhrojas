import {NavigationContainer} from '@react-navigation/native';
import  React,{useEffect,useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {View, Text} from 'react-native';
import {Button, Image} from 'native-base';
const axios = require('axios');

function Location({navigation}) {
  const [data, setData] = useState({});
  const [loading, setLoadingData] = useState(false);
  const [carouselComplete, setCorouselComplete]= useState(false)
const key='storage_Key'

  const storeData = async value => {
    try {
      await AsyncStorage.setItem('@storage_Key', 'true');
    } catch (e) {
      // saving error
    }
  };

  useEffect(() => {
    storeData(carouselComplete);
  }, [carouselComplete]);


  return (
 
    <View style={{marginTop:50, alignItems: 'center', justifyContent: 'center'}}>
       <Image size={'230'} source={require('../../assets/location.png')} />
       <Text style={{fontSize:18}}>Camera Access </Text>
       <Text style={{paddingLeft:100,paddingRight:100,paddingBottom:50,paddingTop:30,textAlign: 'center',fontSize:18,alignItems: 'center', justifyContent: 'center'}}>Please allow access to  your camera to take photos</Text>
       
      <Button   isLoading={loading} disabled={loading}   colorScheme ='secondary' style={{marginBottom:30,borderRadius:20,paddingTop:12,paddingBottom:12,paddingRight:60,paddingLeft:60}}onPress={() => navigation.navigate('ListMemesChile') }>Allow </Button>
      <Button colorScheme='dark' size="sm" variant="ghost">
            Cancel
          </Button>
     
    </View>

  );
}
export default Location;
