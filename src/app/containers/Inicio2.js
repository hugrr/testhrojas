import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import {View, Text, Image} from 'react-native';
import {Button} from 'native-base';
import {getmemes} from '../services/memesChile';

function notification({navigation}) {
  return (
    <View
      style={{marginTop: 50, alignItems: 'center', justifyContent: 'center'}}>
      <Image source={require('../../assets/notification.png')} />
      <Text style={{fontSize: 18}}>Camera Access </Text>
      <Text
        style={{
          paddingLeft: 100,
          paddingRight: 100,
          paddingBottom: 50,
          paddingTop: 30,
          textAlign: 'center',
          fontSize: 18,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        Please allow access to your camera to take photos
      </Text>

      <Button
        colorScheme="secondary"
        style={{
          marginBottom: 30,
          borderRadius: 20,
          paddingTop: 12,
          paddingBottom: 12,
          paddingRight: 60,
          paddingLeft: 60,
        }}
        onPress={() => navigation.navigate('Inicio3')}>
        Allow{' '}
      </Button>
      <Button colorScheme="dark" size="sm" variant="ghost">
        Cancel
      </Button>
    </View>
  );
}
export default notification;
