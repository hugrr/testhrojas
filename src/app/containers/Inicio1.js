import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {Button, Image} from 'native-base';
import {getmemes} from '../services/memesChile';
import AsyncStorage from '@react-native-async-storage/async-storage';




function Camera({navigation}) {

const getData = async () => {
  try {
    const value = await AsyncStorage.getItem('@storage_Key')
    if(value !== null) {
      console.log(value)
    }
  } catch(e) {
    console.log(e)
  }
}

useEffect(() => {
  getData();
  console.log('hjolaaa')
}, [])

  return (
 
    <View style={{marginTop:50, alignItems: 'center', justifyContent: 'center'}}>
       <Image source={require('../../assets/camera.png')} />
       <Text style={{fontSize:18}}>Camera Access </Text>
       <Text style={{paddingLeft:100,paddingRight:100,paddingBottom:50,paddingTop:30,textAlign: 'center',fontSize:18,alignItems: 'center', justifyContent: 'center'}}>Please allow access to  your camera to take photos</Text>
       
      <Button  colorScheme ='secondary' style={{marginBottom:30,borderRadius:20,paddingTop:12,paddingBottom:12,paddingRight:60,paddingLeft:60}}onPress={() => navigation.navigate('Inicio2')}>Allow </Button>
      <Button colorScheme='dark' size="sm" variant="ghost">
            Cancel
          </Button>
     
    </View>

  );
}
export default Camera;
