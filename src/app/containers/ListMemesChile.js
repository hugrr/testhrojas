import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Button,
  Image,
  ScrollView,
  FlatList,
  Box,
  AspectRatio,
  Center,
  Stack,
  Heading,
  HStack,
  VStack,
  Input,
} from 'native-base';
import SearchBar from '../components/SearchBar';
const axios = require('axios');

function ListMemesChile({navigation, route}) {
  const [data, setData] = useState([]);
  const [loadMore, setLoadMore] = useState([]);
  const [loadingData, setLoadingData] = useState(true);
  const [inputSearch, setInputSearch] = useState('');
  const [dataSearch, setDataSearch] = useState([]);
  const [loadingDatasearch, setLoadingDatasearch] = useState([]);
  const [loadingMoreData, setLoadingMoreData] = useState(true);

  const getmemes = async () => {
    setLoadingData(true);
    try {
      const response = await axios.get(
        'https://ww.reddit.com/r/chile/new/.json?limit=100',
      );
      const {data, status} = response;
      if (status === 200) {
        let claves = Object.keys(data); // claves = ["nombre", "color", "macho", "edad"]
        for (let i = 0; i < claves.length; i++) {
          let clave = claves[i];
          const children = data[clave].children;
          const dataMemes =
            children &&
            children.filter(
              item => item.data.link_flair_text === 'Shitposting',
            );
          const dataMemesImage =
            dataMemes &&
            dataMemes.filter(item => item.data.post_hint === 'image');
          console.log(dataMemesImage, 'queee?');
          setData(dataMemesImage);
        }
      } else {
        navigation.navigate('Error');
      }
    } catch (error) {
      console.log(error);
      navigation.navigate('Error');
    }
    setLoadingData(false);
    //setCorouselComplete(true);
  };

  const searchPost = async () => {
    setLoadingDatasearch(true);
    try {
      const response = await axios.get(
        `https://www.reddit.com/r/chile/search.json?q=${inputSearch}&limit=100`,
      );
      const {data, status} = response;
      if (status === 200) {
        let claves = Object.keys(data); // claves = ["nombre", "color", "macho", "edad"]
        for (let i = 0; i < claves.length; i++) {
          let clave = claves[i];
          const children = data[clave].children;
          const searchMemes =
            children &&
            children.filter(
              item => item.data.link_flair_text === 'Shitposting',
            );
          const searchMemesImage =
            searchMemes &&
            searchMemes.filter(item => item.data.post_hint === 'image');
          setDataSearch(searchMemesImage);
          console.log(searchMemesImage, 'search  memes mamamama');
        }
      }
    } catch (error) {
      console.log(error);
    }
    setLoadingDatasearch(false);
  };
  useEffect(() => {
    searchPost();
    dataFLatList();

    console.log(inputSearch, dataSearch, 'input ...');
  }, [inputSearch]);

  useEffect(() => {
    getmemes();
    obtenerListado();
  }, []);

  const dataFLatList = () => {
    if (inputSearch.length > 0) {
      return dataSearch;
    }
    return data;
  };

  useEffect(() => {
    if (loadMore && loadingMoreData === false) {
      const memesLoadMore = loadMore.filter(
        item => item.data.link_flair_text === 'Shitposting',
      );
      const imagesloadMore = memesLoadMore.filter(
        item => item.data.post_hint === 'image',
      );
      setData(data.concat(imagesloadMore));
      console.log(imagesloadMore, 'aca memes ');
    }
  }, [loadMore, loadingMoreData]);

  const obtenerListado = () => {
    let posts = Object.keys(data);
    for (let i = 0; i < posts.length; i++) {
      let post = posts[i];
      const children = data[post].children;
      const memes = children && children.filter(item => item.data.link_flair_text === 'Shitposting');
      const images = memes && memes.filter(item => item.data.post_hint === 'image');
      setData(images);
    }
  };
  const handleLoadMore = async () => {
    setLoadingMoreData(true);
    try {
      const response = await axios.get(
        'https://www.reddit.com/r/chile/.json?limit=100&after=t3_sxdgsb',
      );
      const {data, status} = response;
      if (status === 200) {
        let claves = Object.keys(data); // claves = ["nombre", "color", "macho", "edad"]
        for (let i = 0; i < claves.length; i++) {
          let clave = claves[i];
          const children = data[clave].children;
          setLoadMore(children);
          console.log(data, 'loadmore  memes mamamama');
        }
      }
    } catch (error) {
      console.log(error);
    }
    setLoadingMoreData(false);
  };

  const onChangeText= (text) => setInputSearch(text);


  const renderItem = ({item}) => (
    <Box alignItems="center">
      <Box
        maxW="350"
        rounded="lg"
        overflow="hidden"
        marginBottom={7}
        borderColor="coolGray.200"
        borderWidth="1"
        _dark={{
          borderColor: 'coolGray.600',
          backgroundColor: 'gray.700',
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: 'gray.50',
        }}>
        <Box>
          <AspectRatio w="100%" ratio={10 / 10}>
            <Image
              source={{
                uri: item.data.url,
              }}
              alt="image"
            />
          </AspectRatio>
        </Box>
        <HStack>
          <Stack paddingTop={1}>
            <Center>
              <MaterialCommunityIcons
                name={'chevron-up'}
                color={'grey'}
                size={40}
              />
              <Text style={{color: `#a9a9a9`}}>{item.data.num_comments}</Text>
              <MaterialCommunityIcons
                name={'chevron-down'}
                color={'grey'}
                size={40}
              />
            </Center>
          </Stack>

          <Stack p="4" space={3}>
            <Text style={{fontSize: 18, paddingRight: 40}}>
              {item.data.title}
            </Text>
            <HStack
              alignItems="center"
              space={4}
              justifyContent="space-between">
              <HStack alignItems="center" paddingTop={5}>
                <MaterialCommunityIcons
                  name={'message-processing'}
                  color={`#dcdcdc`}
                  size={30}
                />
                <Text style={{color: `#a9a9a9`, paddingLeft: 10}}>
                  {item.data.score}
                </Text>
              </HStack>
            </HStack>
          </Stack>
        </HStack>
      </Box>
    </Box>
  );

  const renderFlatListMemes = () => {
    if (
      (dataSearch && dataSearch.length !== 0) ||
      (data && data.length > 0 && inputSearch.length === 0)
    ) {
      return (
        <FlatList
          data={dataFLatList()}
          onEndReached={handleLoadMore}
          renderItem={renderItem}
        />
      );
    }
    return (
      <MaterialCommunityIcons
        name={'message-processing'}
        color={`#dcdcdc`}
        size={60}
      />
    );
  };

  return (
    <View>
      <View alignItems={'flex-start'} marginTop={10} marginLeft={10}>
        <MaterialCommunityIcons
          onPress={() => navigation.navigate('Inicio1')}
          name="cog-outline"
          color={'grey'}
          size={40}
        />
      </View>
      {<SearchBar onChangeText={onChangeText} />}
      {!loadingData && renderFlatListMemes()}
    </View>
  );
}

export default ListMemesChile;
