import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Button,
  Image,
  ScrollView,
  FlatList,
  Box,
  AspectRatio,
  Center,
  Stack,
  Heading,
  HStack,
  VStack,
  Input,
} from 'native-base';

const SearchBar = ({onChangeText}) => {
  return (
    <VStack w="100%" space={5} alignSelf="center" padding={5}>
      <Input
        onChangeText={onChangeText}
        placeholder="Search"
        width="100%"
        borderRadius="4"
        py="3"
        px="1"
        fontSize="14"
        InputLeftElement={
          <MaterialCommunityIcons name="magnify" color={'grey'} size={30} />
        }
      />
    </VStack>
  );
};
export default SearchBar;
