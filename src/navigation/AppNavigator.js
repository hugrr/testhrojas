import React, {useState, useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Inicio1 from '../app/containers/Inicio1';
import Inicio2 from '../app/containers/Inicio2';
import Inicio3 from '../app/containers/Inicio3';
import ListMemesChile from '../app/containers/ListMemesChile';
import Error from '../app/containers/Error';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AppNavigator = () => {
  const [permission, setPermission] = useState('');
  const[loadinData,setloadingData] = useState(true)
  const [initialRouteName,setinitialRouteName] = useState('')
  const Stack = createNativeStackNavigator();
  const getData = async () => {
    setloadingData(true);
    try {
      const value = await AsyncStorage.getItem('@storage_Key')
      if(value !== null) {
        console.log(value)
        if (value === "true"){
          setPermission('true');
          console.log('ok')
        }else {setPermission(false);}
      }
    } catch(e) {
      console.log(e)
    }
    setloadingData(false);
  }

  useEffect(() => {
    getData();
  }, [])
  return !loadinData &&(
    <Stack.Navigator initialRouteName={permission ? 'ListMemesChile' : 'Inicio1' }>
      <Stack.Screen name="Inicio1" component={Inicio1} />
      <Stack.Screen name="Inicio2" component={Inicio2} />
      <Stack.Screen name="Inicio3" component={Inicio3} />
      <Stack.Screen name="ListMemesChile" component={ListMemesChile} />
      <Stack.Screen name="Error" component={Error} />
    </Stack.Navigator>
  );
};
export default AppNavigator;
